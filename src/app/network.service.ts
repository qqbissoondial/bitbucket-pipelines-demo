import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class NetworkService {

  constructor(private http: HttpClient) { }

  getCountries() {
    return this.http.get<object[]>('https://restcountries.eu/rest/v2/all?fields=name;capital;population').pipe(
      map(countries => countries.slice(0, 100))
    );
  }
}
